import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
    input: 'dist/filter.module.js',

    output: {
        name: 'angular.filters',
        sourcemap:true,
        format: 'es',
        file:'dist/filter.module.esm.js'
    },

    external: [
        '@angular/core',
        '@angular/common',
        '@angular/forms',
        '@angular/platform-browser',
        '@angular/platform-browser-dynamic',
        '@angular/router',
        'core-js',
        'rxjs/Rx',
        'zone.js'
    ],

    plugins: [
        resolve({
            module: true,
            main: true
        }),
        commonjs({
            include: 'node_modules/**'
        })
    ],
    onwarn: warning => {
        const skip_codes = [
            'THIS_IS_UNDEFINED',
            'MISSING_GLOBAL_NAME'
        ];
        if (skip_codes.indexOf(warning.code) != -1) return;
        console.error(warning);
    }
};
