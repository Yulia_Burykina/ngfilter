/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {Observable, BehaviorSubject} from 'rxjs/Rx';
import {AbstractChildFieldStrategy} from '../interfaces/abstract-child-field-strategy';
import {FilterField} from '../interfaces/filter-field';
import {HierarchicFilterStrategyParams} from '../interfaces/hierarchic-filter-strategy-params';

/**
 * This strategy is applicable for hierarchical lists of options,
 * when child field options are stored in each parent field option
 */
@Injectable()
export class HierarchicFilterStrategy extends AbstractChildFieldStrategy {

    process(field: FilterField, childField: FilterField, options: HierarchicFilterStrategyParams): Observable<FilterField> {

        let subj = new BehaviorSubject<FilterField>(null);

        let children: Array<Object> = null;

        if (typeof field.options !== 'undefined') {
            field.optionsKey = field.optionsKey || 'id';
            field.options.forEach((option) => {

                //when child field options change, we will
                //still keep child field value if possible
                if (option[field.optionsKey] == field.value) {
                    children = option[options.childrenPath] || null;
                }
            });
        }
        childField.options = children;

        subj.next(childField);
        subj.complete();

        return subj.asObservable();
    }

}


