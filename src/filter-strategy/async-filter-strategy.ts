/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';
import {AbstractChildFieldStrategy} from '../interfaces/abstract-child-field-strategy';
import {FilterField} from '../interfaces/filter-field';
import {AsyncFilterStrategyParams} from '../interfaces/async-filter-strategy-params';

/**
 * This strategy is applicable when parent field value change should trigger
 * child field options reload via provider service, e.g. when you want to send
 * http request for new child field options
 */
@Injectable()
export class AsyncFilterStrategy extends AbstractChildFieldStrategy {

    process(field: FilterField, childField: FilterField, params: AsyncFilterStrategyParams): Observable<FilterField> {
        let childProvider = params.optionsProvider;

        let filter = {};

        let optionKey = childField.optionsKey || 'id';

        let subj = new Subject<FilterField>();

        filter[params.param] = params.paramsMap ? params.paramsMap.get(field.value) : field.value;

        childProvider.get(filter).subscribe(
            options => {

                //when child field options change, we will
                //still keep child field value if possible
                let dropValue = true;
                childField.options = options;
                options.forEach(
                    option => {
                        if (option[optionKey] == childField.value) {
                            dropValue = false;
                        }
                    }
                );
                if (dropValue) {
                    childField.value = '';
                }
                subj.next(childField);
                subj.complete();
            }
        );
        return subj.asObservable();
    }

}


