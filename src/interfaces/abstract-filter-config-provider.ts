/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Injectable} from '@angular/core';
import {FilterField} from './filter-field';

/**
 * Extend this class to define configuration for your filter fields
 */
@Injectable()
export abstract class AbstractFilterConfigProvider {
    abstract get(token?: string): Array<FilterField>;
}