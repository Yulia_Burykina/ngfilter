/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Observable} from 'rxjs/Rx';

/**
 * Override this class to preprocess filter field options if needed,
 * e.g. to modify their view or add some data to them
 */
export abstract class AbstractOptionsProcessor<T> {
    abstract process(options: T[]): Observable<T[]>
}