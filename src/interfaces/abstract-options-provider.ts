/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Observable} from 'rxjs/Rx';

/**
 * Override this class to define strategy for getting filter field options,
 * e.g. via http
 */
export abstract class AbstractOptionsProvider<T> {
    abstract get(filter?: {[index: string]: any }): Observable<T[]>;
}