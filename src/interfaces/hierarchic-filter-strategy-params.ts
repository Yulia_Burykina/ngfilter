/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface HierarchicFilterStrategyParams {

    /**
     * Parent field option structure:
     * {
     *    id:..
     *    title:...
     *    [childrenPath]: [
     *      {
     *        id:..
     *        title:..
     *      },
     *    ]
     * }
     */
    childrenPath: string
}