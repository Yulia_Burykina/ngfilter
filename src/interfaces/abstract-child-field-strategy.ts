/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {FilterField} from './filter-field';
import {Observable} from 'rxjs/Rx';

/**
 * Class that defines strategy of getting options for child filter field
 * when parent field value changes. You can use a pre-defined strategy or write your own
 */
export abstract class AbstractChildFieldStrategy {
    abstract process(field: FilterField, childField: FilterField, params: Object): Observable<FilterField>
}