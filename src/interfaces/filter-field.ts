/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Type} from '@angular/core';
import {AbstractOptionsProcessor} from './abstract-options-processor';
import {AbstractOptionsProvider} from './abstract-options-provider';
import {AbstractChildFieldStrategy} from './abstract-child-field-strategy';

export interface FilterField {
    // name attribute value
    name: string;

    // field label
    label: string;

    // field width, %, is added to classname as a modifier, e.g. 25 -> filter__field_25
    width: number;

    // field type or custom component
    type: string | Type<Component>;

    // field attributes
    attributes?: {
        [index: string]: any
    };

    // field placeholder
    placeholder?: string;

    // field value
    value?: any;

    // Class that provides the options via get method
    optionsProvider?: AbstractOptionsProvider<Object>;

    // field options
    //TODO: add the possibility to restrict field values/options
    // even if the field type is not 'select'
    options?: Array<Object>;

    // option field that will be used as key in <option value="key">{{value}}</option>, default: 'id'
    optionsKey?: string;

    // option field that will be used as value in <option value="key">{{value}}</option>, default: 'title'
    optionsValue?: string;

    // dependent fields
    children?: Map<string/*name*/, AbstractChildFieldStrategy>;

    // options preprocessor
    optionsProcessor?: AbstractOptionsProcessor<any>;

    // do we try to preserve values of children after parent change, default: false
    keepValues?: boolean;

    // parameters for children update strategies
    childrenParams?: Map<string/*name*/, Object>;

    // default value
    defaultValue?: string;

    // default filter for options provider
    defaultFilter?: Object;
}