/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {AbstractOptionsProvider} from './abstract-options-provider';

export interface AsyncFilterStrategyParams {

    /**
     * Name of parameter for AbstractOptionsProvider.get({parameter: value})
     * (parent field value will be used as value).
     */
    param: string

    /**
     * Define this to use custom values instead of
     * certain parent field values
     */
    paramsMap?: Map<string, string>

    /**
     * Class that provides the options via get method
     */
    optionsProvider: AbstractOptionsProvider<Object>
}