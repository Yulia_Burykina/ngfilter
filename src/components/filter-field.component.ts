/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, Output, EventEmitter} from '@angular/core';
import {FilterField} from '../interfaces/filter-field';

@Component({
    selector: 'ave-filter-field',
    template: `
        <input
            *ngIf="getTypeof(field.type) === 'string' && field.type !== 'select'"
            [type]="field.type"
            [ngModel]="field.value"
            (ngModelChange)="emitOnChange($event)"
            placeholder="{{field.placeholder}}"
        />
        <select
            *ngIf="field.type === 'select'"
            [ngModel]="field.value"
            (ngModelChange)="emitOnChange($event)"
        >
            <option value="" [selected]="field.value == ''" *ngIf="field.placeholder">{{field.placeholder}}</option>
            <option *ngFor="let opt of field.options" value="{{opt[field.optionsKey || 'id']}}">
                {{opt[field.optionsValue || 'title']}}
            </option>
        </select>
        <div *ngIf="getTypeof(field.type) !== 'string'">
            <ave-control
                [type]="field.type"
                [model]="field.value"
                [field]="field"
                (modelChange)="emitOnChange($event)"
            ></ave-control>
        </div>
    `
})
export class FilterFieldComponent {

    @Input() field: FilterField;
    @Input() fieldvalue: any;
    @Output() onChange = new EventEmitter<FilterField>();

    getTypeof(item: any) {
        return typeof item;
    }

    constructor() {
    }

    emitOnChange(value: any) {
        this.field.value = value;
        this.onChange.emit(this.field);
    }
}