/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FilterField} from '../interfaces/filter-field';
import {AbstractFilterConfigProvider} from '../interfaces/abstract-filter-config-provider';

@Component({
    selector: 'ave-filter',
    template: `
        <div class="filter">
            <div class="filter__field filter__field_{{item.width}}" *ngFor="let item of fields">
                <label>{{item.label}}</label>
                <ave-filter-field [field]="item" [fieldvalue]="item.value"
                                  (onChange)="onChildChange($event)">
                </ave-filter-field>
            </div>

            <div class="filter__buttons">
                <button (click)="onSubmit()" class="filter__button">Применить</button>
                <button (click)="reset()" class="filter__button filter__button_reset">Сбросить</button>
            </div>
        </div>`
})
export class FilterComponent implements OnInit {

    fields: Array<FilterField>;

    fieldsIndex = new Map<string, number>();

    @Input('config') filterConfig: string;

    @Input() page: string;

    @Output('filter') onFilterChange = new EventEmitter<Object>();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private filterConfigProvider: AbstractFilterConfigProvider
    ) { };

    onChildChange(field: FilterField) {
        this.handleChildren(field, false);
    }

    handleChildren(field: FilterField, keepValues?: boolean) {
        keepValues = keepValues || false;

        if (!field.children || field.children.size == 0) {
            return;
        }

        field.children.forEach(
            (strategy, name) => {

                let index = this.fieldsIndex.get(name);

                let childField = this.fields[index];

                if (!childField.keepValues && !keepValues) {
                    childField.value = null;
                }

                let params = field.childrenParams.get(name);
                strategy.process(field, childField, params).subscribe(
                    modifiedChildField => {
                        childField = modifiedChildField;

                        if (modifiedChildField.value !== childField.value) {
                            this.onSubmit();
                        }
                        this.handleChildren(childField, keepValues);
                    }
                );

            }
        );

    }

    populateOptions() {
        //TODO: get rid of callback hell
        this.fields.forEach(
            (field) => {
                if (field.optionsProvider) {
                    let provider = field.optionsProvider;
                    let filter = {};
                    if (field.defaultFilter) {
                        filter = Object.assign({}, filter, field.defaultFilter);
                    }
                    provider.get(filter).subscribe(options => {

                        if (!options) return;
                        if (field.optionsProcessor) {
                            let processor = field.optionsProcessor;
                            processor.process(options).subscribe(
                                options => {
                                    if (!options) return;
                                    field.options = options;
                                    this.handleChildren(field, true);
                                }
                            );
                        } else {
                            field.options = options;
                            this.handleChildren(field, true);
                        }

                    });
                } else {
                    this.handleChildren(field, true);
                }
            }
        )
    }

    ngOnInit() {
        this.fields = this.filterConfigProvider.get(this.filterConfig);
        this.fields.forEach((item, index) => {
            this.fieldsIndex.set(item.name, index);
            item.value = item.defaultValue || '';
            //needs to be defined for usage in onChange parameter
            if (typeof item.keepValues === 'undefined') {
                item = Object.assign({}, item, {keepValues: false});
            }
        });

        this.route.params.subscribe(params => {
            for (let i in params) {
                if (this.fieldsIndex.has(i)) {
                    let field = this.fields[this.fieldsIndex.get(i)];
                    if (typeof field['value'] === 'undefined') continue;
                    field.value = params[i];
                }
            }
            this.populateOptions();
        });
    }

    onSubmit() {
        let filter = {};
        this.fields.forEach((value) => {
            if (value.value) {
                filter[value.name] = value.value;
            }
        });
        return this.router.navigate([this.page, filter]);
    }

    reset() {
        for (let field of this.fields) {
            field.value = null;
        }
    }
}


