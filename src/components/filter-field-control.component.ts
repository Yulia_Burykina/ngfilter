/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    Component, Input, Type, ComponentFactoryResolver, SimpleChanges,
    Output, EventEmitter, ViewContainerRef, ComponentRef, ViewChild,
    OnChanges, AfterViewInit, OnDestroy
} from '@angular/core';

import {FilterField} from '../interfaces/filter-field';

/**
 * Field wrapper that allows usage of custom components as filter fields
 */
@Component({
    selector: 'ave-control',
    template: `<div #target></div>`
})
export class FilterFieldControlComponent implements OnChanges, AfterViewInit, OnDestroy {
    @ViewChild('target', {read: ViewContainerRef}) target: ViewContainerRef;
    @Input() type: Type<Component>;
    @Input() model: any;
    @Input() field: FilterField;
    @Output() modelChange = new EventEmitter<any>();

    cmpRef: ComponentRef<Component>;
    private isViewInitialized: boolean = false;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {}

    updateComponent() {
        if (!this.isViewInitialized) {
            return;
        }
        if (!this.cmpRef) {
            let factory = this.componentFactoryResolver.resolveComponentFactory(this.type);
            this.cmpRef = this.target.createComponent(factory);
            // to access the created instance use
            this.cmpRef.instance['registerOnChange'](
                val => {
                    this.modelChange.emit(val);
                }
            );
            this.cmpRef.instance['params'] = this.field.attributes;
        }
        this.cmpRef.instance['writeValue'](this.model);
        this.cmpRef.changeDetectorRef.detectChanges();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.updateComponent();
    }

    ngAfterViewInit() {
        this.isViewInitialized = true;
        this.updateComponent();
    }

    ngOnDestroy() {
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }
    }
}