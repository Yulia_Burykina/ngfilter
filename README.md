# Angular 5 Filter Fields

A filter (a set of filter fields) that uses Angular router parameters to filter entities, thus allowing to share a link to a filtered set of entities.
Supports dependent fields that are populated using different strategies (you can use a predefined strategy or implement your own).
Supports fields defined as a custom Angular Component.

### Installation 

Add to your package.json dependencies

```
"ngfilter": "git+ssh://git@bitbucket.org/Yulia_Burykina/ngfilter.git"
```

and run npm install. Add AveFilterModule to 'imports' section of your Angular module.

### Usage

```
<ave-filter [page]="'yourPageRoutingName'" [config]="'yourConfigToken'"></ave-filter>
```

1) Configure fields of your filter: extend *AbstractFilterConfigProvider* class like this (pass your custom options providers through constructor if needed):

```
@Injectable()
export class FilterConfig extends AbstractFilterConfigProvider {

    get(token: string) {
	
		// using tokens you can configure as many filters in your app as you like,
		// but there can be only one filter config provider per Angular module
        if(token === 'yourConfigToken') return this.getYourFilter();
    }

    getYourFilter() {
        let map = Array<FilterField>()

        map.push({
            name:'search',
            type: 'text',
            placeholder:"Text search",
            label: 'Enter search phrase'
        });
        return map;
    }
}
```

Add your config to 'providers' section of your Angular module:

```
providers: [
	{provide: AbstractFilterConfigProvider, useClass: FilterConfig},
]
```

2) Extend *AbstractOptionsProvider* to provide options for your 'select' field in a custom way, e.g. via http request:

```
@Injectable()
export class YourProvider extends AbstractOptionsProvider<Object> {

    get(filter?: {[index: string]: any }): Observable<Object[]> {
        // retrieve your options here and return them as an Observable
		// so you can subscribe to them;
		// use 'defaultFilter' property in your config
		// to pass filter Object in this method
    }

}
```

Add them to your config like this:

```
	getYourFilter() {
		//...

		map.push({
		    name:'tag0',
		    type: 'select',
		    placeholder:"Text search",
		    optionsProvider: this.yourProvider
		});
		return map;
	}
```

Don't forget to add all your providers to 'providers' section of your Angular module! 

3) Configure your dependent filter fields using built-in strategies to retrieve their options: 
*HierarchicFilterStrategy* for parent fields whose options already contain options for their children and
*AsyncFilterStrategy* to call *AbstractOptionsProvider* subclasses each time parent value changes. Do it like this:

```
	getYourFilter() {
		//...

		let tag1Children = new Map<string, any>();

		// add chosen strategy to 'providers'
		// and pass it through constructor
		tag1Children.set('tag2', this.hierarchicalFilterStrategy);

		let tag1ChildrenParams = new Map<string, any>();
		tag1ChildrenParams.set('tag2', {
		    childrenPath: 'children'
		});
		
		map.push({
		    name: 'tag1',
		    type: 'select',
		    label: 'Select tag 1',
		    options: [
				{
					id:1,
					title:'section1',
					children: [/* an array of child options corresponding to this option */]
				    }, //...
		    ]
		    children: tag1Children,
		    childrenParams: tag1ChildrenParams,
		    placeholder: "All"
		});
		map.push({
		    name: 'tag2',
		    type: 'select',
		    label: 'Select tag 2',
		    placeholder: "All"
		});

		let section1Children = new Map<string, any>();
		section1Children.set('section2', this.asyncFilterStrategy);

		let section1ChildrenParams = new Map<string, any>();
		section1ChildrenParams.set('section2', {
			// the filter to YourCustomProvider will look like
			// {id: 5}, where 5 is the value 'section1' field
			param: 'id',
			optionsProvider: this.yourCustomProvider
		});

		map.push({
			name:'section1',
			type: 'select',
			label: 'Section 1',
			placeholder: "All",
			options: [/*...*/],
			children: section1Children,
			childrenParams: section1ChildrenParams,
		});
		map.push({
			name: 'section2',
			type: 'select',
			label: 'Select section 2',
			placeholder: "All"
		});	
	}
```

Or implement your own strategy by extending *AbstractChildFieldStrategy* class.

4) You can use your custom component with this filter like this:

```
	getYourFilter() {

		//...

		map.push({
			name:'datetime',
			attributes: {timezone:'+03:00'},
			type: YourDateTimePickerComponent,
			label: 'Date From'
        });
	}
```



