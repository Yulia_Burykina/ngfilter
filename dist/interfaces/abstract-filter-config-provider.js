/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Injectable } from '@angular/core';
/**
 * Extend this class to define configuration for your filter fields
 */
var AbstractFilterConfigProvider = /** @class */ (function () {
    function AbstractFilterConfigProvider() {
    }
    AbstractFilterConfigProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AbstractFilterConfigProvider.ctorParameters = function () { return []; };
    return AbstractFilterConfigProvider;
}());
export { AbstractFilterConfigProvider };
//# sourceMappingURL=abstract-filter-config-provider.js.map