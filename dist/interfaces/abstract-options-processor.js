/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Override this class to preprocess filter field options if needed,
 * e.g. to modify their view or add some data to them
 */
var /**
 * Override this class to preprocess filter field options if needed,
 * e.g. to modify their view or add some data to them
 */
AbstractOptionsProcessor = /** @class */ (function () {
    function AbstractOptionsProcessor() {
    }
    return AbstractOptionsProcessor;
}());
/**
 * Override this class to preprocess filter field options if needed,
 * e.g. to modify their view or add some data to them
 */
export { AbstractOptionsProcessor };
//# sourceMappingURL=abstract-options-processor.js.map