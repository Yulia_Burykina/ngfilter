import { FilterField } from './filter-field';
/**
 * Extend this class to define configuration for your filter fields
 */
export declare abstract class AbstractFilterConfigProvider {
    abstract get(token?: string): Array<FilterField>;
}
