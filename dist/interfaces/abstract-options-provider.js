/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Override this class to define strategy for getting filter field options,
 * e.g. via http
 */
var /**
 * Override this class to define strategy for getting filter field options,
 * e.g. via http
 */
AbstractOptionsProvider = /** @class */ (function () {
    function AbstractOptionsProvider() {
    }
    return AbstractOptionsProvider;
}());
/**
 * Override this class to define strategy for getting filter field options,
 * e.g. via http
 */
export { AbstractOptionsProvider };
//# sourceMappingURL=abstract-options-provider.js.map