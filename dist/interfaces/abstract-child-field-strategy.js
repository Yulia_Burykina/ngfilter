/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Class that defines strategy of getting options for child filter field
 * when parent field value changes. You can use a pre-defined strategy or write your own
 */
var /**
 * Class that defines strategy of getting options for child filter field
 * when parent field value changes. You can use a pre-defined strategy or write your own
 */
AbstractChildFieldStrategy = /** @class */ (function () {
    function AbstractChildFieldStrategy() {
    }
    return AbstractChildFieldStrategy;
}());
/**
 * Class that defines strategy of getting options for child filter field
 * when parent field value changes. You can use a pre-defined strategy or write your own
 */
export { AbstractChildFieldStrategy };
//# sourceMappingURL=abstract-child-field-strategy.js.map