/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component, Type } from '@angular/core';
import { AbstractOptionsProcessor } from './abstract-options-processor';
import { AbstractOptionsProvider } from './abstract-options-provider';
import { AbstractChildFieldStrategy } from './abstract-child-field-strategy';
export interface FilterField {
    name: string;
    label: string;
    width: number;
    type: string | Type<Component>;
    attributes?: {
        [index: string]: any;
    };
    placeholder?: string;
    value?: any;
    optionsProvider?: AbstractOptionsProvider<Object>;
    options?: Array<Object>;
    optionsKey?: string;
    optionsValue?: string;
    children?: Map<string, AbstractChildFieldStrategy>;
    optionsProcessor?: AbstractOptionsProcessor<any>;
    keepValues?: boolean;
    childrenParams?: Map<string, Object>;
    defaultValue?: string;
    defaultFilter?: Object;
}
