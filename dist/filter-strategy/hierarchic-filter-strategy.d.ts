import { Observable } from 'rxjs/Rx';
import { AbstractChildFieldStrategy } from '../interfaces/abstract-child-field-strategy';
import { FilterField } from '../interfaces/filter-field';
import { HierarchicFilterStrategyParams } from '../interfaces/hierarchic-filter-strategy-params';
/**
 * This strategy is applicable for hierarchical lists of options,
 * when child field options are stored in each parent field option
 */
export declare class HierarchicFilterStrategy extends AbstractChildFieldStrategy {
    process(field: FilterField, childField: FilterField, options: HierarchicFilterStrategyParams): Observable<FilterField>;
}
