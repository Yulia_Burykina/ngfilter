var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { AbstractChildFieldStrategy } from '../interfaces/abstract-child-field-strategy';
/**
 * This strategy is applicable for hierarchical lists of options,
 * when child field options are stored in each parent field option
 */
var HierarchicFilterStrategy = /** @class */ (function (_super) {
    __extends(HierarchicFilterStrategy, _super);
    function HierarchicFilterStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HierarchicFilterStrategy.prototype.process = function (field, childField, options) {
        var subj = new BehaviorSubject(null);
        var children = null;
        if (typeof field.options !== 'undefined') {
            field.optionsKey = field.optionsKey || 'id';
            field.options.forEach(function (option) {
                //when child field options change, we will
                //still keep child field value if possible
                if (option[field.optionsKey] == field.value) {
                    children = option[options.childrenPath] || null;
                }
            });
        }
        childField.options = children;
        subj.next(childField);
        subj.complete();
        return subj.asObservable();
    };
    HierarchicFilterStrategy.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    HierarchicFilterStrategy.ctorParameters = function () { return []; };
    return HierarchicFilterStrategy;
}(AbstractChildFieldStrategy));
export { HierarchicFilterStrategy };
//# sourceMappingURL=hierarchic-filter-strategy.js.map