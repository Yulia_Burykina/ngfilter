import { Observable } from 'rxjs/Rx';
import { AbstractChildFieldStrategy } from '../interfaces/abstract-child-field-strategy';
import { FilterField } from '../interfaces/filter-field';
import { AsyncFilterStrategyParams } from '../interfaces/async-filter-strategy-params';
/**
 * This strategy is applicable when parent field value change should trigger
 * child field options reload via provider service, e.g. when you want to send
 * http request for new child field options
 */
export declare class AsyncFilterStrategy extends AbstractChildFieldStrategy {
    process(field: FilterField, childField: FilterField, params: AsyncFilterStrategyParams): Observable<FilterField>;
}
