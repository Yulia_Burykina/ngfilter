var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Rx';
import { AbstractChildFieldStrategy } from '../interfaces/abstract-child-field-strategy';
/**
 * This strategy is applicable when parent field value change should trigger
 * child field options reload via provider service, e.g. when you want to send
 * http request for new child field options
 */
var AsyncFilterStrategy = /** @class */ (function (_super) {
    __extends(AsyncFilterStrategy, _super);
    function AsyncFilterStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AsyncFilterStrategy.prototype.process = function (field, childField, params) {
        var childProvider = params.optionsProvider;
        var filter = {};
        var optionKey = childField.optionsKey || 'id';
        var subj = new Subject();
        filter[params.param] = params.paramsMap ? params.paramsMap.get(field.value) : field.value;
        childProvider.get(filter).subscribe(function (options) {
            //when child field options change, we will
            //still keep child field value if possible
            var dropValue = true;
            childField.options = options;
            options.forEach(function (option) {
                if (option[optionKey] == childField.value) {
                    dropValue = false;
                }
            });
            if (dropValue) {
                childField.value = '';
            }
            subj.next(childField);
            subj.complete();
        });
        return subj.asObservable();
    };
    AsyncFilterStrategy.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AsyncFilterStrategy.ctorParameters = function () { return []; };
    return AsyncFilterStrategy;
}(AbstractChildFieldStrategy));
export { AsyncFilterStrategy };
//# sourceMappingURL=async-filter-strategy.js.map