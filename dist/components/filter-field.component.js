/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
var FilterFieldComponent = /** @class */ (function () {
    function FilterFieldComponent() {
        this.onChange = new EventEmitter();
    }
    FilterFieldComponent.prototype.getTypeof = function (item) {
        return typeof item;
    };
    FilterFieldComponent.prototype.emitOnChange = function (value) {
        this.field.value = value;
        this.onChange.emit(this.field);
    };
    FilterFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ave-filter-field',
                    template: "\n        <input\n            *ngIf=\"getTypeof(field.type) === 'string' && field.type !== 'select'\"\n            [type]=\"field.type\"\n            [ngModel]=\"field.value\"\n            (ngModelChange)=\"emitOnChange($event)\"\n            placeholder=\"{{field.placeholder}}\"\n        />\n        <select\n            *ngIf=\"field.type === 'select'\"\n            [ngModel]=\"field.value\"\n            (ngModelChange)=\"emitOnChange($event)\"\n        >\n            <option value=\"\" [selected]=\"field.value == ''\" *ngIf=\"field.placeholder\">{{field.placeholder}}</option>\n            <option *ngFor=\"let opt of field.options\" value=\"{{opt[field.optionsKey || 'id']}}\">\n                {{opt[field.optionsValue || 'title']}}\n            </option>\n        </select>\n        <div *ngIf=\"getTypeof(field.type) !== 'string'\">\n            <ave-control\n                [type]=\"field.type\"\n                [model]=\"field.value\"\n                [field]=\"field\"\n                (modelChange)=\"emitOnChange($event)\"\n            ></ave-control>\n        </div>\n    "
                },] },
    ];
    /** @nocollapse */
    FilterFieldComponent.ctorParameters = function () { return []; };
    FilterFieldComponent.propDecorators = {
        "field": [{ type: Input },],
        "fieldvalue": [{ type: Input },],
        "onChange": [{ type: Output },],
    };
    return FilterFieldComponent;
}());
export { FilterFieldComponent };
//# sourceMappingURL=filter-field.component.js.map