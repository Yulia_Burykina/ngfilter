/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component, Type, ComponentFactoryResolver, SimpleChanges, EventEmitter, ViewContainerRef, ComponentRef, OnChanges, AfterViewInit, OnDestroy } from '@angular/core';
import { FilterField } from '../interfaces/filter-field';
/**
 * Field wrapper that allows usage of custom components as filter fields
 */
export declare class FilterFieldControlComponent implements OnChanges, AfterViewInit, OnDestroy {
    private componentFactoryResolver;
    target: ViewContainerRef;
    type: Type<Component>;
    model: any;
    field: FilterField;
    modelChange: EventEmitter<any>;
    cmpRef: ComponentRef<Component>;
    private isViewInitialized;
    constructor(componentFactoryResolver: ComponentFactoryResolver);
    updateComponent(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
}
