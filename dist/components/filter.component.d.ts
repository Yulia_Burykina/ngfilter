/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { EventEmitter, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FilterField } from '../interfaces/filter-field';
import { AbstractFilterConfigProvider } from '../interfaces/abstract-filter-config-provider';
export declare class FilterComponent implements OnInit {
    private route;
    private router;
    private filterConfigProvider;
    fields: Array<FilterField>;
    fieldsIndex: Map<string, number>;
    filterConfig: string;
    page: string;
    onFilterChange: EventEmitter<Object>;
    constructor(route: ActivatedRoute, router: Router, filterConfigProvider: AbstractFilterConfigProvider);
    onChildChange(field: FilterField): void;
    handleChildren(field: FilterField, keepValues?: boolean): void;
    populateOptions(): void;
    ngOnInit(): void;
    onSubmit(): Promise<boolean>;
    reset(): void;
}
