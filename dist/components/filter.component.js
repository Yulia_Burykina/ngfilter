/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AbstractFilterConfigProvider } from '../interfaces/abstract-filter-config-provider';
var FilterComponent = /** @class */ (function () {
    function FilterComponent(route, router, filterConfigProvider) {
        this.route = route;
        this.router = router;
        this.filterConfigProvider = filterConfigProvider;
        this.fieldsIndex = new Map();
        this.onFilterChange = new EventEmitter();
    }
    ;
    FilterComponent.prototype.onChildChange = function (field) {
        this.handleChildren(field, false);
    };
    FilterComponent.prototype.handleChildren = function (field, keepValues) {
        var _this = this;
        keepValues = keepValues || false;
        if (!field.children || field.children.size == 0) {
            return;
        }
        field.children.forEach(function (strategy, name) {
            var index = _this.fieldsIndex.get(name);
            var childField = _this.fields[index];
            if (!childField.keepValues && !keepValues) {
                childField.value = null;
            }
            var params = field.childrenParams.get(name);
            strategy.process(field, childField, params).subscribe(function (modifiedChildField) {
                childField = modifiedChildField;
                if (modifiedChildField.value !== childField.value) {
                    _this.onSubmit();
                }
                _this.handleChildren(childField, keepValues);
            });
        });
    };
    FilterComponent.prototype.populateOptions = function () {
        var _this = this;
        //TODO: get rid of callback hell
        this.fields.forEach(function (field) {
            if (field.optionsProvider) {
                var provider = field.optionsProvider;
                var filter = {};
                if (field.defaultFilter) {
                    filter = Object.assign({}, filter, field.defaultFilter);
                }
                provider.get(filter).subscribe(function (options) {
                    if (!options)
                        return;
                    if (field.optionsProcessor) {
                        var processor = field.optionsProcessor;
                        processor.process(options).subscribe(function (options) {
                            if (!options)
                                return;
                            field.options = options;
                            _this.handleChildren(field, true);
                        });
                    }
                    else {
                        field.options = options;
                        _this.handleChildren(field, true);
                    }
                });
            }
            else {
                _this.handleChildren(field, true);
            }
        });
    };
    FilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fields = this.filterConfigProvider.get(this.filterConfig);
        this.fields.forEach(function (item, index) {
            _this.fieldsIndex.set(item.name, index);
            item.value = item.defaultValue || '';
            //needs to be defined for usage in onChange parameter
            if (typeof item.keepValues === 'undefined') {
                item = Object.assign({}, item, { keepValues: false });
            }
        });
        this.route.params.subscribe(function (params) {
            for (var i in params) {
                if (_this.fieldsIndex.has(i)) {
                    var field = _this.fields[_this.fieldsIndex.get(i)];
                    if (typeof field['value'] === 'undefined')
                        continue;
                    field.value = params[i];
                }
            }
            _this.populateOptions();
        });
    };
    FilterComponent.prototype.onSubmit = function () {
        var filter = {};
        this.fields.forEach(function (value) {
            if (value.value) {
                filter[value.name] = value.value;
            }
        });
        return this.router.navigate([this.page, filter]);
    };
    FilterComponent.prototype.reset = function () {
        for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
            var field = _a[_i];
            field.value = null;
        }
    };
    FilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ave-filter',
                    template: "\n        <div class=\"filter\">\n            <div class=\"filter__field filter__field_{{item.width}}\" *ngFor=\"let item of fields\">\n                <label>{{item.label}}</label>\n                <ave-filter-field [field]=\"item\" [fieldvalue]=\"item.value\"\n                                  (onChange)=\"onChildChange($event)\">\n                </ave-filter-field>\n            </div>\n\n            <div class=\"filter__buttons\">\n                <button (click)=\"onSubmit()\" class=\"filter__button\">\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C</button>\n                <button (click)=\"reset()\" class=\"filter__button filter__button_reset\">\u0421\u0431\u0440\u043E\u0441\u0438\u0442\u044C</button>\n            </div>\n        </div>"
                },] },
    ];
    /** @nocollapse */
    FilterComponent.ctorParameters = function () { return [
        { type: ActivatedRoute, },
        { type: Router, },
        { type: AbstractFilterConfigProvider, },
    ]; };
    FilterComponent.propDecorators = {
        "filterConfig": [{ type: Input, args: ['config',] },],
        "page": [{ type: Input },],
        "onFilterChange": [{ type: Output, args: ['filter',] },],
    };
    return FilterComponent;
}());
export { FilterComponent };
//# sourceMappingURL=filter.component.js.map