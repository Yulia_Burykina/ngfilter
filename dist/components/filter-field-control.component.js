/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component, Input, Type, ComponentFactoryResolver, Output, EventEmitter, ViewContainerRef, ViewChild } from '@angular/core';
/**
 * Field wrapper that allows usage of custom components as filter fields
 */
var FilterFieldControlComponent = /** @class */ (function () {
    function FilterFieldControlComponent(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.modelChange = new EventEmitter();
        this.isViewInitialized = false;
    }
    FilterFieldControlComponent.prototype.updateComponent = function () {
        var _this = this;
        if (!this.isViewInitialized) {
            return;
        }
        if (!this.cmpRef) {
            var factory = this.componentFactoryResolver.resolveComponentFactory(this.type);
            this.cmpRef = this.target.createComponent(factory);
            // to access the created instance use
            this.cmpRef.instance['registerOnChange'](function (val) {
                _this.modelChange.emit(val);
            });
            this.cmpRef.instance['params'] = this.field.attributes;
        }
        this.cmpRef.instance['writeValue'](this.model);
        this.cmpRef.changeDetectorRef.detectChanges();
    };
    FilterFieldControlComponent.prototype.ngOnChanges = function (changes) {
        this.updateComponent();
    };
    FilterFieldControlComponent.prototype.ngAfterViewInit = function () {
        this.isViewInitialized = true;
        this.updateComponent();
    };
    FilterFieldControlComponent.prototype.ngOnDestroy = function () {
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }
    };
    FilterFieldControlComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ave-control',
                    template: "<div #target></div>"
                },] },
    ];
    /** @nocollapse */
    FilterFieldControlComponent.ctorParameters = function () { return [
        { type: ComponentFactoryResolver, },
    ]; };
    FilterFieldControlComponent.propDecorators = {
        "target": [{ type: ViewChild, args: ['target', { read: ViewContainerRef },] },],
        "type": [{ type: Input },],
        "model": [{ type: Input },],
        "field": [{ type: Input },],
        "modelChange": [{ type: Output },],
    };
    return FilterFieldControlComponent;
}());
export { FilterFieldControlComponent };
//# sourceMappingURL=filter-field-control.component.js.map