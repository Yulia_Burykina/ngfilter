/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { EventEmitter } from '@angular/core';
import { FilterField } from '../interfaces/filter-field';
export declare class FilterFieldComponent {
    field: FilterField;
    fieldvalue: any;
    onChange: EventEmitter<FilterField>;
    getTypeof(item: any): "string" | "number" | "boolean" | "symbol" | "undefined" | "object" | "function";
    constructor();
    emitOnChange(value: any): void;
}
