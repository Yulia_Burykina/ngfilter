import { Injectable, Component, Input, Output, EventEmitter, ComponentFactoryResolver, ViewContainerRef, ViewChild, NgModule } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject, BehaviorSubject } from 'rxjs/Rx';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Extend this class to define configuration for your filter fields
 */
var AbstractFilterConfigProvider = /** @class */ (function () {
    function AbstractFilterConfigProvider() {
    }
    AbstractFilterConfigProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AbstractFilterConfigProvider.ctorParameters = function () { return []; };
    return AbstractFilterConfigProvider;
}());

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
var FilterComponent = /** @class */ (function () {
    function FilterComponent(route, router, filterConfigProvider) {
        this.route = route;
        this.router = router;
        this.filterConfigProvider = filterConfigProvider;
        this.fieldsIndex = new Map();
        this.onFilterChange = new EventEmitter();
    }
    FilterComponent.prototype.onChildChange = function (field) {
        this.handleChildren(field, false);
    };
    FilterComponent.prototype.handleChildren = function (field, keepValues) {
        var _this = this;
        keepValues = keepValues || false;
        if (!field.children || field.children.size == 0) {
            return;
        }
        field.children.forEach(function (strategy, name) {
            var index = _this.fieldsIndex.get(name);
            var childField = _this.fields[index];
            if (!childField.keepValues && !keepValues) {
                childField.value = null;
            }
            var params = field.childrenParams.get(name);
            strategy.process(field, childField, params).subscribe(function (modifiedChildField) {
                childField = modifiedChildField;
                if (modifiedChildField.value !== childField.value) {
                    _this.onSubmit();
                }
                _this.handleChildren(childField, keepValues);
            });
        });
    };
    FilterComponent.prototype.populateOptions = function () {
        var _this = this;
        //TODO: get rid of callback hell
        this.fields.forEach(function (field) {
            if (field.optionsProvider) {
                var provider = field.optionsProvider;
                var filter = {};
                if (field.defaultFilter) {
                    filter = Object.assign({}, filter, field.defaultFilter);
                }
                provider.get(filter).subscribe(function (options) {
                    if (!options)
                        return;
                    if (field.optionsProcessor) {
                        var processor = field.optionsProcessor;
                        processor.process(options).subscribe(function (options) {
                            if (!options)
                                return;
                            field.options = options;
                            _this.handleChildren(field, true);
                        });
                    }
                    else {
                        field.options = options;
                        _this.handleChildren(field, true);
                    }
                });
            }
            else {
                _this.handleChildren(field, true);
            }
        });
    };
    FilterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.fields = this.filterConfigProvider.get(this.filterConfig);
        this.fields.forEach(function (item, index) {
            _this.fieldsIndex.set(item.name, index);
            item.value = item.defaultValue || '';
            //needs to be defined for usage in onChange parameter
            if (typeof item.keepValues === 'undefined') {
                item = Object.assign({}, item, { keepValues: false });
            }
        });
        this.route.params.subscribe(function (params) {
            for (var i in params) {
                if (_this.fieldsIndex.has(i)) {
                    var field = _this.fields[_this.fieldsIndex.get(i)];
                    if (typeof field['value'] === 'undefined')
                        continue;
                    field.value = params[i];
                }
            }
            _this.populateOptions();
        });
    };
    FilterComponent.prototype.onSubmit = function () {
        var filter = {};
        this.fields.forEach(function (value) {
            if (value.value) {
                filter[value.name] = value.value;
            }
        });
        return this.router.navigate([this.page, filter]);
    };
    FilterComponent.prototype.reset = function () {
        for (var _i = 0, _a = this.fields; _i < _a.length; _i++) {
            var field = _a[_i];
            field.value = null;
        }
    };
    FilterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ave-filter',
                    template: "\n        <div class=\"filter\">\n            <div class=\"filter__field filter__field_{{item.width}}\" *ngFor=\"let item of fields\">\n                <label>{{item.label}}</label>\n                <ave-filter-field [field]=\"item\" [fieldvalue]=\"item.value\"\n                                  (onChange)=\"onChildChange($event)\">\n                </ave-filter-field>\n            </div>\n\n            <div class=\"filter__buttons\">\n                <button (click)=\"onSubmit()\" class=\"filter__button\">\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C</button>\n                <button (click)=\"reset()\" class=\"filter__button filter__button_reset\">\u0421\u0431\u0440\u043E\u0441\u0438\u0442\u044C</button>\n            </div>\n        </div>"
                },] },
    ];
    /** @nocollapse */
    FilterComponent.ctorParameters = function () { return [
        { type: ActivatedRoute, },
        { type: Router, },
        { type: AbstractFilterConfigProvider, },
    ]; };
    FilterComponent.propDecorators = {
        "filterConfig": [{ type: Input, args: ['config',] },],
        "page": [{ type: Input },],
        "onFilterChange": [{ type: Output, args: ['filter',] },],
    };
    return FilterComponent;
}());

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Field wrapper that allows usage of custom components as filter fields
 */
var FilterFieldControlComponent = /** @class */ (function () {
    function FilterFieldControlComponent(componentFactoryResolver) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.modelChange = new EventEmitter();
        this.isViewInitialized = false;
    }
    FilterFieldControlComponent.prototype.updateComponent = function () {
        var _this = this;
        if (!this.isViewInitialized) {
            return;
        }
        if (!this.cmpRef) {
            var factory = this.componentFactoryResolver.resolveComponentFactory(this.type);
            this.cmpRef = this.target.createComponent(factory);
            // to access the created instance use
            this.cmpRef.instance['registerOnChange'](function (val) {
                _this.modelChange.emit(val);
            });
            this.cmpRef.instance['params'] = this.field.attributes;
        }
        this.cmpRef.instance['writeValue'](this.model);
        this.cmpRef.changeDetectorRef.detectChanges();
    };
    FilterFieldControlComponent.prototype.ngOnChanges = function (changes) {
        this.updateComponent();
    };
    FilterFieldControlComponent.prototype.ngAfterViewInit = function () {
        this.isViewInitialized = true;
        this.updateComponent();
    };
    FilterFieldControlComponent.prototype.ngOnDestroy = function () {
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }
    };
    FilterFieldControlComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ave-control',
                    template: "<div #target></div>"
                },] },
    ];
    /** @nocollapse */
    FilterFieldControlComponent.ctorParameters = function () { return [
        { type: ComponentFactoryResolver, },
    ]; };
    FilterFieldControlComponent.propDecorators = {
        "target": [{ type: ViewChild, args: ['target', { read: ViewContainerRef },] },],
        "type": [{ type: Input },],
        "model": [{ type: Input },],
        "field": [{ type: Input },],
        "modelChange": [{ type: Output },],
    };
    return FilterFieldControlComponent;
}());

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
var FilterFieldComponent = /** @class */ (function () {
    function FilterFieldComponent() {
        this.onChange = new EventEmitter();
    }
    FilterFieldComponent.prototype.getTypeof = function (item) {
        return typeof item;
    };
    FilterFieldComponent.prototype.emitOnChange = function (value) {
        this.field.value = value;
        this.onChange.emit(this.field);
    };
    FilterFieldComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ave-filter-field',
                    template: "\n        <input\n            *ngIf=\"getTypeof(field.type) === 'string' && field.type !== 'select'\"\n            [type]=\"field.type\"\n            [ngModel]=\"field.value\"\n            (ngModelChange)=\"emitOnChange($event)\"\n            placeholder=\"{{field.placeholder}}\"\n        />\n        <select\n            *ngIf=\"field.type === 'select'\"\n            [ngModel]=\"field.value\"\n            (ngModelChange)=\"emitOnChange($event)\"\n        >\n            <option value=\"\" [selected]=\"field.value == ''\" *ngIf=\"field.placeholder\">{{field.placeholder}}</option>\n            <option *ngFor=\"let opt of field.options\" value=\"{{opt[field.optionsKey || 'id']}}\">\n                {{opt[field.optionsValue || 'title']}}\n            </option>\n        </select>\n        <div *ngIf=\"getTypeof(field.type) !== 'string'\">\n            <ave-control\n                [type]=\"field.type\"\n                [model]=\"field.value\"\n                [field]=\"field\"\n                (modelChange)=\"emitOnChange($event)\"\n            ></ave-control>\n        </div>\n    "
                },] },
    ];
    /** @nocollapse */
    FilterFieldComponent.ctorParameters = function () { return []; };
    FilterFieldComponent.propDecorators = {
        "field": [{ type: Input },],
        "fieldvalue": [{ type: Input },],
        "onChange": [{ type: Output },],
    };
    return FilterFieldComponent;
}());

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Class that defines strategy of getting options for child filter field
 * when parent field value changes. You can use a pre-defined strategy or write your own
 */
var  /**
 * Class that defines strategy of getting options for child filter field
 * when parent field value changes. You can use a pre-defined strategy or write your own
 */
AbstractChildFieldStrategy = /** @class */ (function () {
    function AbstractChildFieldStrategy() {
    }
    return AbstractChildFieldStrategy;
}());

var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * This strategy is applicable when parent field value change should trigger
 * child field options reload via provider service, e.g. when you want to send
 * http request for new child field options
 */
var AsyncFilterStrategy = /** @class */ (function (_super) {
    __extends(AsyncFilterStrategy, _super);
    function AsyncFilterStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AsyncFilterStrategy.prototype.process = function (field, childField, params) {
        var childProvider = params.optionsProvider;
        var filter = {};
        var optionKey = childField.optionsKey || 'id';
        var subj = new Subject();
        filter[params.param] = params.paramsMap ? params.paramsMap.get(field.value) : field.value;
        childProvider.get(filter).subscribe(function (options) {
            //when child field options change, we will
            //still keep child field value if possible
            var dropValue = true;
            childField.options = options;
            options.forEach(function (option) {
                if (option[optionKey] == childField.value) {
                    dropValue = false;
                }
            });
            if (dropValue) {
                childField.value = '';
            }
            subj.next(childField);
            subj.complete();
        });
        return subj.asObservable();
    };
    AsyncFilterStrategy.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AsyncFilterStrategy.ctorParameters = function () { return []; };
    return AsyncFilterStrategy;
}(AbstractChildFieldStrategy));

var __extends$1 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * This strategy is applicable for hierarchical lists of options,
 * when child field options are stored in each parent field option
 */
var HierarchicFilterStrategy = /** @class */ (function (_super) {
    __extends$1(HierarchicFilterStrategy, _super);
    function HierarchicFilterStrategy() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    HierarchicFilterStrategy.prototype.process = function (field, childField, options) {
        var subj = new BehaviorSubject(null);
        var children = null;
        if (typeof field.options !== 'undefined') {
            field.optionsKey = field.optionsKey || 'id';
            field.options.forEach(function (option) {
                //when child field options change, we will
                //still keep child field value if possible
                if (option[field.optionsKey] == field.value) {
                    children = option[options.childrenPath] || null;
                }
            });
        }
        childField.options = children;
        subj.next(childField);
        subj.complete();
        return subj.asObservable();
    };
    HierarchicFilterStrategy.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    HierarchicFilterStrategy.ctorParameters = function () { return []; };
    return HierarchicFilterStrategy;
}(AbstractChildFieldStrategy));

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Override this class to preprocess filter field options if needed,
 * e.g. to modify their view or add some data to them
 */
var  /**
 * Override this class to preprocess filter field options if needed,
 * e.g. to modify their view or add some data to them
 */
AbstractOptionsProcessor = /** @class */ (function () {
    function AbstractOptionsProcessor() {
    }
    return AbstractOptionsProcessor;
}());

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/**
 * Override this class to define strategy for getting filter field options,
 * e.g. via http
 */
var  /**
 * Override this class to define strategy for getting filter field options,
 * e.g. via http
 */
AbstractOptionsProvider = /** @class */ (function () {
    function AbstractOptionsProvider() {
    }
    return AbstractOptionsProvider;
}());

/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
var AveFilterModule = /** @class */ (function () {
    function AveFilterModule() {
    }
    AveFilterModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        BrowserModule,
                        FormsModule
                    ],
                    declarations: [
                        FilterComponent,
                        FilterFieldControlComponent,
                        FilterFieldComponent
                    ],
                    exports: [
                        FilterComponent,
                        FilterFieldControlComponent,
                        FilterFieldComponent
                    ],
                    providers: []
                },] },
    ];
    /** @nocollapse */
    AveFilterModule.ctorParameters = function () { return []; };
    return AveFilterModule;
}());

export { AveFilterModule, FilterComponent, FilterFieldControlComponent, FilterFieldComponent, AsyncFilterStrategy, HierarchicFilterStrategy, AbstractChildFieldStrategy, AbstractFilterConfigProvider, AbstractOptionsProcessor, AbstractOptionsProvider };
//# sourceMappingURL=filter.module.esm.js.map
