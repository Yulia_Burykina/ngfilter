/**
 * @fileoverview This file was generated by the Angular template compiler. Do not edit.
 *
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.

 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride,checkTypes}
 * tslint:disable
 */ 
import * as i0 from "@angular/core";
import * as i1 from "./filter.module";
import * as i2 from "@angular/common";
import * as i3 from "@angular/platform-browser";
import * as i4 from "@angular/forms";
var AveFilterModuleNgFactory = i0.ɵcmf(i1.AveFilterModule, [], function (_l) { return i0.ɵmod([i0.ɵmpd(512, i0.ComponentFactoryResolver, i0.ɵCodegenComponentFactoryResolver, [[8, []], [3, i0.ComponentFactoryResolver], i0.NgModuleRef]), i0.ɵmpd(5120, i0.LOCALE_ID, i0.ɵq, [[3, i0.LOCALE_ID]]), i0.ɵmpd(4608, i2.NgLocalization, i2.NgLocaleLocalization, [i0.LOCALE_ID, [2, i2.ɵa]]), i0.ɵmpd(4608, i0.Compiler, i0.Compiler, []), i0.ɵmpd(5120, i0.APP_ID, i0.ɵi, []), i0.ɵmpd(5120, i0.IterableDiffers, i0.ɵn, []), i0.ɵmpd(5120, i0.KeyValueDiffers, i0.ɵo, []), i0.ɵmpd(4608, i3.DomSanitizer, i3.ɵe, [i2.DOCUMENT]), i0.ɵmpd(6144, i0.Sanitizer, null, [i3.DomSanitizer]), i0.ɵmpd(4608, i3.HAMMER_GESTURE_CONFIG, i3.HammerGestureConfig, []), i0.ɵmpd(5120, i3.EVENT_MANAGER_PLUGINS, function (p0_0, p0_1, p1_0, p2_0, p2_1) { return [new i3.ɵDomEventsPlugin(p0_0, p0_1), new i3.ɵKeyEventsPlugin(p1_0), new i3.ɵHammerGesturesPlugin(p2_0, p2_1)]; }, [i2.DOCUMENT, i0.NgZone, i2.DOCUMENT, i2.DOCUMENT, i3.HAMMER_GESTURE_CONFIG]), i0.ɵmpd(4608, i3.EventManager, i3.EventManager, [i3.EVENT_MANAGER_PLUGINS, i0.NgZone]), i0.ɵmpd(135680, i3.ɵDomSharedStylesHost, i3.ɵDomSharedStylesHost, [i2.DOCUMENT]), i0.ɵmpd(4608, i3.ɵDomRendererFactory2, i3.ɵDomRendererFactory2, [i3.EventManager, i3.ɵDomSharedStylesHost]), i0.ɵmpd(6144, i0.RendererFactory2, null, [i3.ɵDomRendererFactory2]), i0.ɵmpd(6144, i3.ɵSharedStylesHost, null, [i3.ɵDomSharedStylesHost]), i0.ɵmpd(4608, i0.Testability, i0.Testability, [i0.NgZone]), i0.ɵmpd(4608, i3.Meta, i3.Meta, [i2.DOCUMENT]), i0.ɵmpd(4608, i3.Title, i3.Title, [i2.DOCUMENT]), i0.ɵmpd(4608, i4.ɵi, i4.ɵi, []), i0.ɵmpd(512, i2.CommonModule, i2.CommonModule, []), i0.ɵmpd(1024, i0.ErrorHandler, i3.ɵa, []), i0.ɵmpd(1024, i0.APP_INITIALIZER, function (p0_0) { return [i3.ɵh(p0_0)]; }, [[2, i0.NgProbeToken]]), i0.ɵmpd(512, i0.ApplicationInitStatus, i0.ApplicationInitStatus, [[2, i0.APP_INITIALIZER]]), i0.ɵmpd(131584, i0.ApplicationRef, i0.ApplicationRef, [i0.NgZone, i0.ɵConsole, i0.Injector, i0.ErrorHandler, i0.ComponentFactoryResolver, i0.ApplicationInitStatus]), i0.ɵmpd(512, i0.ApplicationModule, i0.ApplicationModule, [i0.ApplicationRef]), i0.ɵmpd(512, i3.BrowserModule, i3.BrowserModule, [[3, i3.BrowserModule]]), i0.ɵmpd(512, i4.ɵba, i4.ɵba, []), i0.ɵmpd(512, i4.FormsModule, i4.FormsModule, []), i0.ɵmpd(512, i1.AveFilterModule, i1.AveFilterModule, [])]); });
export { AveFilterModuleNgFactory as AveFilterModuleNgFactory };
//# sourceMappingURL=filter.module.ngfactory.js.map