/**
 * Copyright 2017, Yulia Burykina <burykina.j.i@gmail.com>.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { FilterComponent } from './components/filter.component';
import { FilterFieldControlComponent } from './components/filter-field-control.component';
import { FilterFieldComponent } from './components/filter-field.component';
export * from './components/filter.component';
export * from './components/filter-field-control.component';
export * from './components/filter-field.component';
export * from './filter-strategy/async-filter-strategy';
export * from './filter-strategy/hierarchic-filter-strategy';
export * from './interfaces/abstract-child-field-strategy';
export * from './interfaces/abstract-filter-config-provider';
export * from './interfaces/abstract-options-processor';
export * from './interfaces/abstract-options-provider';
var AveFilterModule = /** @class */ (function () {
    function AveFilterModule() {
    }
    AveFilterModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        BrowserModule,
                        FormsModule
                    ],
                    declarations: [
                        FilterComponent,
                        FilterFieldControlComponent,
                        FilterFieldComponent
                    ],
                    exports: [
                        FilterComponent,
                        FilterFieldControlComponent,
                        FilterFieldComponent
                    ],
                    providers: []
                },] },
    ];
    /** @nocollapse */
    AveFilterModule.ctorParameters = function () { return []; };
    return AveFilterModule;
}());
export { AveFilterModule };
//# sourceMappingURL=filter.module.js.map